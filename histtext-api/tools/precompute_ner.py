#!/usr/bin/env python
# coding: utf-8

import argparse
import asyncio
import json
import os
import logging
from typing import Union, Optional

import jsonlines
import spacy
from tqdm import tqdm
from transformers.pipelines import AggregationStrategy

from ..models import ModelInfo, TransformerNER
from ..solr import SolrServer, close_solr_session, start_solr_session

logger = logging.getLogger(__name__)


def argparser() -> argparse.Namespace:
    """Program argument parser. Only call once."""
    parser = argparse.ArgumentParser()
    parser.add_argument('collection')
    parser.add_argument('model_path_or_name')
    parser.add_argument('histtext_model_name')
    parser.add_argument('text_solr_field')
    parser.add_argument('cache_output_dir')
    parser.add_argument('--filter-query')
    parser.add_argument('--jsonl-prefix')
    parser.add_argument('--collection-start', type=int, default=0)
    parser.add_argument('--collection-batch-size', type=int, default=10000)
    parser.add_argument('--collection-nbatches', type=int)
    parser.add_argument('-p', '--decimal-precision', type=int)
    parser.add_argument('-s', '--shorten', action="store_true")
    parser.add_argument('--model-max-length', type=int)
    parser.add_argument('--aggregation-strategy', default="FIRST")
    parser.add_argument('--solr-host', default="localhost")
    parser.add_argument('--solr-port', default=8983)
    parser.add_argument('--gpu', action="store_true")
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


nerkeys2short = {
    "text": "t",
    "labels": "l",
    "start_pos": "s",
    "end_pos": "e",
    "confidence": "c"
}


nerlabels2short = {
    "PERSON": "P",
    "PER": "P",
    "NORP": "N",
    "FAC": "F",
    "ORG": "O",
    "GPE": "G",
    "LOC": "L",
    "PRODUCT": "PR",
    "EVENT": "E",
    "WORK_OF_ART": "W",
    "LAW": "LA",
    "DATE": "D",
    "TIME": "T",
    "PERCENT": "PE",
    "MONEY": "M",
    "QUANTITY": "Q",
    "ORDINAL": "OR",
    "CARDINAL": "C",
    "LANGUAGE": "LG",
    "MISC": "MI"
}


async def get_document_batch(solr_server: SolrServer, collection: str,
                             text_field: str,
                             start: int, batch_size: int,
                             filter_query: Optional[str] = None) -> dict[str, str]:
    payload = {
        "q": "*:*",
        "rows": batch_size,
        "start": start
    }
    if filter_query is not None:
        payload["fq"] = filter_query

    select = await solr_server.collection_select(collection, payload)
    if not select["response"]["docs"]:
        return []
    return {doc["id"]: doc.get(text_field, "") for doc in select["response"]["docs"]}


def normalized_entity(text: str, labels: list[str],
                      start_pos: int, end_pos: int,
                      confidence: float,
                      shorten: bool = False,
                      decimal_precision: Optional[int] = None) -> dict:
    def shorten_key(key: str) -> str:
        return key if not shorten else nerkeys2short[key]

    def shorten_labels(labels: list[str]) -> list[str]:
        return labels if not shorten else list(map(lambda x: nerlabels2short[x], labels))

    entity = {
        shorten_key("text"): text,
        shorten_key("labels"): shorten_labels(labels),
        shorten_key("start_pos"): start_pos,
        shorten_key("end_pos"): end_pos,
    }

    if confidence >= 0:
        if decimal_precision is not None:
            confidence = round(confidence, decimal_precision)
        entity[shorten_key("confidence")] = confidence
    return entity


def split_long_document(doc: str) -> list[str]:
    splits = doc.split("\n")
    doc_splits = []
    cur_doc_split = ""
    for split in splits:
        if len(cur_doc_split) + len(split) + 1 <= 30000:
            cur_doc_split += "\n" + split
        else:
            doc_splits.append(cur_doc_split)
            cur_doc_split = "\n" + split
    if cur_doc_split:
        doc_splits.append(cur_doc_split)
    return doc_splits

def spacy_ner(ner_model: spacy.Language,
              documents: dict[str, str],
              shorten: bool = False):
    results = {}
    doc_list = list(documents.items())
    for doc_idx in range(len(doc_list)):
        docid, doc = doc_list[doc_idx]
        if len(doc) > 30000:
            doc_splits = split_long_document(doc)
            doc_list[doc_idx] = (docid, doc_splits[0])
            doc_list.extend((docid, doc_split) for doc_split in doc_splits[1:])
    doc_offsets = {}
    for ner_doc, info in tqdm(ner_model.pipe(((b.replace("\n", " "), (a, b)) for a, b in doc_list),
                                             as_tuples=True, batch_size=1)):
        # ner_doc = ner_model(doc)
        doc_id, doc_str = info
        result = []
        for entity in ner_doc.ents:
            result.append(
                normalized_entity(
                    entity.text, [entity.label_],
                    doc_offsets.get(doc_id, 0) + entity.start_char,
                    doc_offsets.get(doc_id, 0) + entity.end_char,
                    -1.0, shorten=shorten
                )
            )
        if doc_id not in results:
            results[doc_id] = result
            doc_offsets[doc_id] = len(doc_str)
        else:
            results[doc_id].extend(result)
            doc_offsets[doc_id] += len(doc_str)
    return results


def transformers_pipeline(ner_model: TransformerNER,
                          documents: dict[str, str],
                          shorten: bool = False,
                          decimal_precision: Optional[int] = None):
    results = {}
    for doc_id, doc in tqdm(documents.items()):
        ner_output = ner_model(doc.replace("\n", " ")) if doc else []
        result = []
        for entity in ner_output:
            result.append(
                normalized_entity(
                    entity["word"], [entity["entity_group"]],
                    int(entity["start"]), int(entity["end"]),
                    float(entity["score"]),
                    shorten=shorten,
                    decimal_precision=decimal_precision
                )
            )
        results[doc_id] = result
    return results


def apply_ner_on_documents(ner_model: Union[TransformerNER, spacy.Language],
                           documents: dict[str, str],
                           shorten: bool = False,
                           decimal_precision: Optional[int] = None):
    if isinstance(ner_model, TransformerNER):
        return transformers_pipeline(ner_model, documents,
                                     shorten=shorten,
                                     decimal_precision=decimal_precision)
    else:
        return spacy_ner(ner_model, documents, shorten=shorten)


async def apply_ner_on_collection(solr_server: SolrServer, collection: str,
                                  text_field: str,
                                  coll_start: int, coll_batch_size: int,
                                  ner_model: Union[TransformerNER, spacy.Language],
                                  jsonl_out: jsonlines.Writer,
                                  filter_query: Optional[str] = None,
                                  shorten: bool = False,
                                  decimal_precision: Optional[int] = None) -> int:
    documents = await get_document_batch(solr_server, collection, text_field,
                                         coll_start, coll_batch_size,
                                         filter_query=filter_query)
    logger.debug("Retrieved %d documents.", len(documents))
    if not documents:
        return []
    entities = apply_ner_on_documents(ner_model, documents,
                                      shorten=shorten,
                                      decimal_precision=decimal_precision)

    annot_key = "annotation" if not shorten else "a"
    entities_jsons = [{"id": doc_id, annot_key: ner_result}
                      for doc_id, ner_result in entities.items()]
    jsonl_out.write_all(entities_jsons)
    return [entity["id"] for entity in entities_jsons]


def load_index(annot_path: str):
    if not os.path.exists(f"{annot_path}/index.json"):
        return {}
    with open(f"{annot_path}/index.json", 'r') as fin:
        return json.load(fin)


def dump_index(annot_path: str, index: dict[str, tuple[str, int]]):
    with open(f"{annot_path}/index.json", 'w') as fout:
        json.dump(index, fout, ensure_ascii=False)


async def main():
    args = argparser()

    solr_server = SolrServer(args.solr_host, args.solr_port)
    start_solr_session()

    if args.histtext_model_name.startswith("trftc"):
        model_info = ModelInfo(args.model_path_or_name, args.model_max_length,
                               AggregationStrategy[args.aggregation_strategy])
        ner_model = TransformerNER(model_info, 10)
    else:
        spacy.require_gpu()
        ner_model = spacy.load(args.model_path_or_name, exclude=["parser", "tagger",
                                                                 "lemmatizer",
                                                                 "attribute_ruler"])

    cache_path = os.path.abspath(os.path.expanduser(os.path.expandvars(args.cache_output_dir)))
    annot_path = f"{cache_path}/{args.histtext_model_name}/{args.collection}/{args.text_solr_field}"
    os.makedirs(annot_path, exist_ok=True)
    index = load_index(annot_path)
    start = args.collection_start
    n_batches = 0
    logger.info("Precomputing...")
    while args.collection_nbatches is None or n_batches < args.collection_nbatches:
        logger.debug("Applying on %d to %d", start, start + args.collection_batch_size - 1)
        if args.jsonl_prefix is None:
            jsonl_basename = f"{start}"
        else:
            jsonl_basename = f"{args.jsonl_prefix}-{start}"
        jsonl_path = f"{annot_path}/{jsonl_basename}.jsonl"
        with jsonlines.open(jsonl_path, 'w') as jsonl_out:
            docids = await apply_ner_on_collection(solr_server, args.collection, args.text_solr_field,
                                                   start, args.collection_batch_size,
                                                   ner_model, jsonl_out, filter_query=args.filter_query,
                                                   shorten=args.shorten,
                                                   decimal_precision=args.decimal_precision)
            for docid_idx, docid in enumerate(docids):
                index[docid] = (jsonl_basename, docid_idx)
        dump_index(annot_path, index)
        if len(docids) < args.collection_batch_size:
            logger.info("No documents left in collection.")
            break
        n_batches += 1
        start += args.collection_batch_size

    logger.info("Precomputation and indexation done.")

    await close_solr_session()


if __name__ == "__main__":
    asyncio.run(main())
