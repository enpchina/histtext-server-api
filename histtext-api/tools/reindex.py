#!/usr/bin/env python
# coding: utf-8

import argparse
import logging
import os

import jsonlines
from tqdm import tqdm

from .precompute_ner import dump_index, nerkeys2short, nerlabels2short

logger = logging.getLogger(__name__)


def argparser() -> argparse.Namespace:
    """Program argument parser. Only call once."""
    parser = argparse.ArgumentParser()
    parser.add_argument('cache_dir')
    parser.add_argument('new_cache_dir')
    parser.add_argument('-p', '--decimal-precision', type=int)
    parser.add_argument('-b', '--batch-size', type=int, default=10000)
    parser.add_argument('-s', '--shorten', action="store_true")
    parser.add_argument('-L', '--labels-key', default="labels")
    parser.add_argument('-t', '--annotation-type', choices=["ner"], default="ner")
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


keys2short = {
    "ner": nerkeys2short
}

labels2short = {
    "ner": nerlabels2short
}


def main():
    args = argparser()

    index = {}
    jsonl_idx = 0
    jsonl_out = None
    sample_idx = 0

    for filename in tqdm(os.scandir(args.cache_dir)):
        if filename.is_dir():
            continue
        if not filename.name.endswith(".jsonl"):
            continue
        if jsonl_out is None:
            jsonl_out = jsonlines.open(f"{args.new_cache_dir}/{jsonl_idx}.jsonl", 'w')
        with jsonlines.open(filename.path, 'r') as jsonl_in:
            for sample_data in jsonl_in:
                annot_key = "a" if args.shorten else "annotation"
                new_data = {
                    "id": sample_data["id"],
                    annot_key: []
                }
                for annot in sample_data["annotation"]:
                    new_annot = {}
                    for key, value in annot.items():
                        if args.decimal_precision is not None and isinstance(value, float):
                            value = round(value, args.decimal_precision)
                        if args.shorten and key == args.labels_key:
                            if isinstance(value, list):
                                value = list(map(lambda x: labels2short[args.annotation_type][x], value))
                            else:
                                value = labels2short[args.annotation_type][value]
                        new_annot[keys2short[args.annotation_type][key] if args.shorten else key] = value
                    new_data[annot_key].append(new_annot)
                jsonl_out.write(new_data)
                index[new_data["id"]] = (f"{jsonl_idx}", sample_idx)
                sample_idx += 1
                if sample_idx % args.batch_size == 0:
                    jsonl_out.close()
                    jsonl_idx += 1
                    sample_idx = 0
                    jsonl_out = jsonlines.open(f"{args.new_cache_dir}/{jsonl_idx}.jsonl", 'w')
    jsonl_out.close()

    dump_index(args.new_cache_dir, index)


if __name__ == "__main__":
    main()
