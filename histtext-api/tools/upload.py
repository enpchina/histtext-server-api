#!/usr/bin/env python
# coding: utf-8

import argparse
import logging
import yaml
import jsonlines
import asyncio
from typing import Iterator, Any
from ..solr import SolrServer, start_solr_session, close_solr_session


logger = logging.getLogger(__name__)


def argparser() -> argparse.Namespace:
    """Program argument parser. Only call once."""
    parser = argparse.ArgumentParser()
    parser.add_argument('collection_name')
    parser.add_argument('documents_jsonl', nargs='+')
    parser.add_argument('--schema')
    parser.add_argument('--solr-host', default="localhost")
    parser.add_argument('--solr-port', default=8983)
    parser.add_argument('-b', '--batch-size', type=int, default=1000)
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


def split_in_json(jsonl_in: jsonlines.Reader,
                  batch_size: int) -> Iterator[list[dict]]:
    docs_json = []
    for doc_idx, doc_json in enumerate(jsonl_in, start=1):
        docs_json.append(doc_json)
        if doc_idx % batch_size == 0:
            yield docs_json
            docs_json = []
    if docs_json:
        yield docs_json


async def setup_schema(solr_server: SolrServer, collection: str,
                       types: list[str], fields: dict[str, dict[str, Any]]):
    logger.info("Creating collection '%s'...", collection)
    create_resp = await solr_server.create_collection(collection)
    if not create_resp["responseHeader"] != 0:
        raise RuntimeError("Can't create collection... Already exists?")
    for type_ in types:
        logger.info("Adding field type '%s' to '%s'...", type_, collection)
        if type_ == "rdate":
            await solr_server.add_daterange_field_type(collection)
        elif type_ == "text_normalized_cjk":
            await solr_server.add_cjk_field_type(collection)
        else:
            raise ValueError(f"Unknown field type: {type_}")

    for field, params in fields.items():
        logger.info("Adding field '%s' to '%s'...", field, collection)
        await solr_server.add_field(collection, field, params["type"],
                                    params.get("indexed", True), params.get("stored", True),
                                    params.get("multivalued", False))


async def main():
    args = argparser()

    solr_server = SolrServer(args.solr_host, args.solr_port)
    start_solr_session()

    if args.schema is not None:
        with open(args.schema, 'r') as yaml_in:
            schema_yaml = yaml.safe_load(yaml_in)
        _ = await setup_schema(solr_server, args.collection_name,
                               schema_yaml["schema"]["types"],
                               schema_yaml["schema"]["fields"])

    for document_jsonl in args.documents_jsonl:
        logger.info("Loading '%s'...", document_jsonl)
        with jsonlines.open(document_jsonl, 'r') as jsonl_in:
            for json_batch in split_in_json(jsonl_in, args.batch_size):
                _ = await solr_server.upload_documents(args.collection_name,
                                                       json_batch)
                logger.info("Sent %d documents... ('%s' to '%s')", len(json_batch),
                            json_batch[0]["id"], json_batch[-1]["id"])

    await close_solr_session()


if __name__ == "__main__":
    asyncio.run(main())
