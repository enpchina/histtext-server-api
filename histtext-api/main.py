from fastapi import Depends, FastAPI

from .dependencies import get_solr_server, list_corpora
from .routers import corpora, ner, cws, qa
from .solr import SolrServer, close_solr_session, start_solr_session

app = FastAPI()


@app.on_event("startup")
async def startup_event():
    start_solr_session()


@app.on_event("shutdown")
async def shutdown_event():
    await close_solr_session()


@app.get("/")
def root(solr_server: SolrServer = Depends(get_solr_server)):
    solr_status = solr_server.status(list_corpora()[0])
    return {"status": solr_status}


@app.get("/corpora")
async def get_corpora(solr_server: SolrServer = Depends(get_solr_server)):
    spec_corpora = set(list_corpora())
    solr_corpora = set(await solr_server.list_corpora())
    return list(sorted(spec_corpora & solr_corpora))


app.include_router(corpora.router)
app.include_router(ner.router)
app.include_router(cws.router)
app.include_router(qa.router)
