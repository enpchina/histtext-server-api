"""Solr-related class and functions."""

import requests
import aiohttp
from dataclasses import dataclass
from enum import Enum
from typing import Optional


class SolrOperator(Enum):
    """Enumeration of Solr query operators."""

    SolrOr = "OR"
    SolrAnd = "AND"


class SolrRangeGapType(Enum):
    """Enumeration of Solr range gap types."""

    SolrDay = "DAY"
    SolrMonth = "MONTH"
    SolrYear = "YEAR"


@dataclass
class QueryOptions:
    """Options for Solr search queries."""

    ctxt: Optional[int]
    sf: list[str]
    sfop: SolrOperator
    fq: list[str]
    fqop: SolrOperator
    date_field: Optional[str]
    date: list[str]
    maxrows: int
    start: int


solr_session = None


def start_solr_session():
    """Start the client session used to connect to the Solr server."""
    global solr_session
    solr_session = aiohttp.ClientSession()


async def close_solr_session():
    """Close the client session used to connect to the Solr server."""
    await solr_session.close()


class SolrServer:
    """Class that eases interactions with a Solr server."""

    url: str

    def __init__(self, hostname: str, port: int):
        """Construct a new SolrServer instance."""
        self.url = f"http://{hostname}:{port}/solr"

    def status(self, collection: str) -> str:
        """Get the collection status of the Solr server."""
        try:
            response = requests.get(f"{self.url}/{collection}/admin/ping")
            return response.json()["status"]
        except requests.exceptions.ConnectionError:
            return "Solr Unreachable"

    async def admin_request(self, request: str, params: dict[str, str],
                            raise_for_status: bool = False):
        """Make an admin request to the Solr server."""
        async with solr_session.get(f"{self.url}/admin/{request}", params=params) as response:
            if raise_for_status:
                response.raise_for_status()
            return await response.json()

    async def collection_select(self, collection: str, params: dict[str, str],
                                raise_for_status: bool = False):
        """Make a request on a collection to the Solr server."""
        async with solr_session.get(f"{self.url}/{collection}/select", params=params) as response:
            if raise_for_status:
                response.raise_for_status()
            return await response.json()

    async def list_corpora(self):
        """List the available collection aliases in the Solr server."""
        aliases = (await self.admin_request("collections", {"action": "listAliases"}))["aliases"]
        return list(aliases.keys())

    async def query_all_field_values(self, collection: str, field: str):
        """Retrieve the unique values (with count) for a given field in a collection."""
        payload = {
            "facet.field": field,
            "facet.limit": -1,
            "facet": "true",
            "q": "*:*"
        }
        select = await self.collection_select(collection, payload)
        try:
            values = select["facet_counts"]["facet_fields"][field]
        except KeyError:
            return {}
        return {values[idx]: values[idx + 1] for idx in range(0, len(values), 2)}

    async def count_number_documents(self, collection: str, query: str, options: QueryOptions):
        """Count the number of documents to expect from a query in a collection."""
        q = f" {options.sfop.value} ".join(f"{field}:({query})" for field in options.sf)
        payload = {
            "q": q,
            "rows": 0,
            "start": 0
        }

        date_query = None
        if options.date_field is not None and options.date:
            date_query = " OR ".join(f"{options.date_field}:({date})" for date in options.date)
        filter_query = None
        if options.fq:
            filter_query = f" {options.fqop.value} ".join(options.fq)

        filters = [filter_query, date_query]
        fq = " AND ".join(filter(lambda x: x is not None, filters))
        if filters:
            payload["fq"] = fq

        select = await self.collection_select(collection, payload)
        try:
            return select["response"]["numFound"]
        except KeyError:
            return 0

    async def search_collection(self, collection: str, query: str, options: QueryOptions):
        """Search a collection from a query."""
        q = f" {options.sfop.value} ".join(f"{field}:({query})" for field in options.sf)
        payload = {
            "q": q,
            "rows": options.maxrows,
            "start": options.start,
        }

        date_query = None
        if options.date_field is not None and options.date:
            date_query = " OR ".join(f"{options.date_field}:({date})" for date in options.date)
        filter_query = None
        if options.fq:
            filter_query = f" {options.fqop.value} ".join(options.fq)

        filters = [filter_query, date_query]
        fq = " AND ".join(filter(lambda x: x is not None, filters))
        if filters:
            payload["fq"] = fq

        if options.ctxt is not None:
            hl_payload = {
                "hl": "true",
                "hl.method": "unified",
                "hl.fl": ",".join(options.sf),
                "hl.snippets": 1000,
                "hl.tag.pre": "<match>",
                "hl.tag.post": "</match>",
                "hl.fragsize": 0  # options.ctxt
            }
            payload.update(hl_payload)

        select = await self.collection_select(collection, payload)
        docs = select["response"]["docs"]
        highlights = None
        if options.ctxt is not None:
            highlights = select["highlighting"]
        return docs, highlights

    async def get_document(self, collection: str, doc_id: str):
        """Retrieve documents from a document ID."""
        payload = {
            "q": f"id:{doc_id}"
        }

        select = await self.collection_select(collection, payload)
        if not select["response"]["docs"]:
            return None
        return select["response"]["docs"][0]

    async def count_docs(self, collection: str, query: str, sf: list[str],
                         sfop: SolrOperator, by_field: str):
        """Group and count documents that match a query."""
        q = f" {sfop.value} ".join(f"{field}:({query})" for field in sf)
        payload = {
            "q": q,
            "facet": "true",
            "facet.field": by_field,
            "facet.limit": -1
        }

        select = await self.collection_select(collection, payload)
        try:
            values = select["facet_counts"]["facet_fields"][by_field]
            return {values[idx]: values[idx + 1] for idx in range(0, len(values), 2)}
        except KeyError:
            return {}

    async def count_dates(self, collection: str, date_start: str, date_end: str,
                          date_gap_type: SolrRangeGapType, date_gap: int,
                          query: str, sf: list[str], sfop: SolrOperator):
        """Group and count documents that match a query by date."""
        def iso_date(date: str) -> Optional[str]:
            date = date.replace("/", "-")
            if len(date) == 4:
                return f"{date}-01-01T00:00:00Z"
            elif len(date) == 7:
                return f"{date}-01T00:00:00Z"
            elif len(date) == 10:
                return f"{date}T00:00:00Z"
            elif len(date) == 20:
                return date
            return None

        def cut_date(date: str) -> str:
            if date_gap_type == SolrRangeGapType.SolrDay:
                return date[:10]
            if date_gap_type == SolrRangeGapType.SolrMonth:
                return date[:7]
            if date_gap_type == SolrRangeGapType.SolrYear:
                return date[:4]
        if (date_start := iso_date(date_start)) is None:
            raise RuntimeError("date_start has wrong format.")
        if (date_end := iso_date(date_end)) is None:
            raise RuntimeError("date_end has wrong format.")
        q = f" {sfop.value} ".join(f"{field}:({query})" for field in sf)
        payload = {
            "q": q,
            "facet": "true",
            "facet.range": "date_rdt",
            "facet.range.start": date_start,
            "facet.range.end": date_end,
            "facet.range.gap": f"+{date_gap}{date_gap_type.value}",
            "facet.limit": -1
        }

        select = await self.collection_select(collection, payload)

        try:
            values = select["facet_counts"]["facet_ranges"]["date_rdt"]["counts"]
            return {cut_date(values[idx]): values[idx + 1] for idx in range(0, len(values), 2)}
        except KeyError:
            return {}

    async def upload_documents(self, collection: str, documents: list[dict]):
        """Upload documents to a collection."""
        async with solr_session.post(f"{self.url}/{collection}/update?commit=true",
                                     json=documents) as response:
            response.raise_for_status()
            return await response.read()

    async def create_collection(self, collection: str, num_shards: int = 1, num_replicas: int = 1) -> bool:
        """Create a new collection on the Solr server."""
        payload = {
            "action": "CREATE",
            "name": collection,
            "numShards": num_shards,
            "replicationFactor": num_replicas
        }
        create = await self.admin_request("collections", payload, True)
        return create

    async def alter_schema(self, collection: str, command: dict):
        """Make a change to a collection's schema."""
        async with solr_session.post(f"{self.url}/{collection}/schema", json=command) as response:
            response.raise_for_status()
            return await response.read()

    async def add_daterange_field_type(self, collection: str):
        """Add the `rdate` field type to a collection."""
        command = {
            "add-field-type": {
                "name": "rdate",
                "class": "solr.DateRangeField"
            }
        }
        return await self.alter_schema(collection, command)

    async def add_cjk_field_type(self, collection: str):
        """Add the `text_normalized_cjk` field type to a collection."""
        command = {
            "add-field-type": {
                "name": "text_normalized_cjk",
                "class": "solr.TextField",
                "analyzer": {
                    "tokenizer": {
                        "class": "solr.StandardTokenizerFactory"
                    },
                    "filters": [{
                        "class": "eu.enpchina.lucene.filter.CJKNormalizerFilterFactory"
                    }]
                }
            }
        }
        return await self.alter_schema(collection, command)

    async def add_field(self, collection: str, name: str, type_: str,
                        indexed: bool = True, stored: bool = True,
                        multivalued: bool = False):
        """Add a new field to a collection."""
        command = {
            "add-field": {
                "name": name,
                "type": type_,
                "indexed": "true" if indexed else "false",
                "stored": "true" if stored else "false",
                "multiValued": "true" if multivalued else "false"
            }
        }
        return await self.alter_schema(collection, command)
