import csv
import logging
from io import StringIO
from typing import Optional

from fastapi import APIRouter, Depends, Request, Response, HTTPException

from ..annotation import (ModelStatus, apply_model, apply_model_on_solr,
                          supported_models)
from ..dependencies import get_corpus, get_solr_server
from ..solr import SolrServer
from ..specifications import CollectionSpecification

logger = logging.getLogger(__name__)
router = APIRouter()


def model_status_to_exception(status: ModelStatus):
    if status == ModelStatus.ModelNotImplemented:
        raise HTTPException(500, "Requested QA model is not correctly implemented on the server.")
    if status == ModelStatus.TooManyUsers:
        raise HTTPException(503, "Too many concurrent users. Please try again later.")
    if status == ModelStatus.OtherModelInUse:
        raise HTTPException(503, "Other model currently in use by other users. Please try again later.")
    if status == ModelStatus.UnknownModel:
        raise HTTPException(404, "Requested QA model is unknown to the server.")


# @router.get("/{collection}-qa/list_precomputed_fields")
# async def list_precomputed_fields(specification: Optional[CollectionSpecification] = Depends(get_corpus)):
#     return list(get_fields_with_precomputed_support("qa", specification.collection))


@router.post("/{collection}-qa/extract")
async def qa_on_corpus(request: Request,
                       model: str,
                       # question: list[str] = Query([]),
                       field: str = "__default__",
                       # max_answers: int = 1,
                       # threshold: float = 0.0,
                       specification: Optional[CollectionSpecification] = Depends(get_corpus),
                       solr_server: SolrServer = Depends(get_solr_server)):
    body = await request.json()
    # docids = filter(lambda x: len(x.strip()) > 0, body.split('\n'))
    questions = body["questions"]
    if isinstance(questions, list):
        questions = {"__notopic__": questions}
    if isinstance(questions, str):
        questions = {"__notopic__": [questions]}
    max_answers = body["max_answers"]
    threshold = body["threshold"]
    docids = body["docids"]

    if model == "__default__":
        raise HTTPException(404, detail="QA doesn't have a default model.")
    if not model.endswith(":qa"):
        raise HTTPException(404, detail="Invalid model name for QA")

    if field == "__default__":
        field = specification.default_field

    str_buf = StringIO()
    writer = csv.writer(str_buf)
    writer.writerow(["DocId", "Type", "Text", "Start", "End", "Score"])
    for docid in docids:
        result = await apply_model_on_solr(solr_server, specification.collection,
                                           field, model, docid, False,
                                           {"questions": questions,
                                            "max_answers": max_answers,
                                            "threshold": threshold})
        if result is None:
            continue
        if result["status"] != ModelStatus.OK:
            str_buf.close()
            model_status_to_exception(result["status"])
            return {"status": result["status"]}
        for topic, answers in result["result"].items():
            for answer_idx, answer in enumerate(answers):
                topic_key = f"{topic}:{answer_idx}" if len(answers) > 1 else topic
                writer.writerow([docid, topic_key, answer["answer"],
                                 answer["start"], answer["end"],
                                 answer.get("score", -1.0)])
    content = str_buf.getvalue()
    str_buf.close()
    return Response(content=content, media_type="text/csv")


@router.get("/qa/list_models")
def list_models():
    return [display_name for display_name, path_name in supported_models.items() if path_name is not None and display_name.endswith(":qa")]


@router.get("/qa/list_precomputed_collections")
def list_precomputed_collections():
    return []


@router.post("/qa/{model}/extract")
async def qa_on_custom(request: Request,
                       model: str):
    if not model.endswith(":qa"):
        raise HTTPException(404, detail="Invalid model name for QA")
    body = await request.json()
    questions = body["questions"]
    if isinstance(questions, list):
        questions = {"__notopic__": questions}
    if isinstance(questions, str):
        questions = {"__notopic__": [questions]}
    max_answers = body["max_answers"]
    threshold = body["threshold"]
    lines = filter(lambda x: len(x.strip()) > 0, body["text"].split('\n'))

    str_buf = StringIO()
    writer = csv.writer(str_buf)
    writer.writerow(["Type", "Text", "Start", "End", "Score"])
    for line in lines:
        result = await apply_model(model, line,
                                   {"question": questions,
                                    "max_answers": max_answers,
                                    "threshold": threshold})
        if result["status"] != ModelStatus.OK:
            str_buf.close()
            model_status_to_exception(result["status"])
            return {"status": result["status"]}
        for topic, answers in result["result"].items():
            for answer_idx, answer in enumerate(answers):
                topic_key = f"{topic}:{answer_idx}" if len(answers) > 1 else topic
                writer.writerow([topic_key, answer["answer"],
                                 answer["start"], answer["end"],
                                 answer.get("score", -1.0)])
    content = str_buf.getvalue()
    str_buf.close()
    return Response(content=content, media_type="text/csv")
