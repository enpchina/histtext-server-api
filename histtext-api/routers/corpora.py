import csv
import itertools
import logging
import xml.etree.ElementTree as et
from dataclasses import asdict
from io import StringIO
from typing import Iterable, Optional

import regex as re
from fastapi import APIRouter, Depends, HTTPException, Query, Request, Response

from ..dependencies import get_corpus, get_solr_server
from ..solr import QueryOptions, SolrOperator, SolrServer, SolrRangeGapType
from ..specifications import CollectionSpecification

logger = logging.getLogger(__name__)
router = APIRouter()


def to_camel_case(text: str, ) -> str:
    parts = text.split("_")
    return "".join(part.capitalize() for part in parts)  # if part != "id" else "ID"


special_chars = ''.join(map(chr, itertools.chain(range(0x00, 0x20), range(0x7f, 0xa0))))
special_chars_re = re.compile('[%s]' % re.escape(special_chars))
def clean_string(text: str) -> str:
    return special_chars_re.sub("", text).strip()


@router.get("/{collection}/list_searchfields")
def list_search_fields(specification: CollectionSpecification = Depends(get_corpus)):
    return specification.search_fields


@router.get("/{collection}/list_filterfields")
def list_filter_fields(specification: CollectionSpecification = Depends(get_corpus)):
    return specification.filter_fields


@router.get("/{collection}/date_support")
def has_date_support(specification: CollectionSpecification = Depends(get_corpus)):
    return specification.date_field is not None


@router.get("/{collection}/default_searchfield")
def get_default_search_field(specification: CollectionSpecification = Depends(get_corpus)):
    return specification.default_field


@router.get("/{collection}/list_possible_filters/{field}")
async def list_possible_filters(field: str,
                                specification: CollectionSpecification = Depends(get_corpus),
                                solr_server: SolrServer = Depends(get_solr_server)):
    if field not in specification.filter_fields and field not in specification.search_fields:
        raise HTTPException(404, f"Unknown field: {field}")

    filters = await solr_server.query_all_field_values(specification.collection, field)

    root = et.Element("Results")
    for value, count in filters.items():
        et.SubElement(root, "Filter", value=value, n=str(count))
    xml_str = et.tostring(root, encoding="unicode", method="xml")

    return Response(content=xml_str, media_type="application/xml")


def search_query(specification: CollectionSpecification = Depends(get_corpus),
                 ctxt: Optional[int] = None,
                 sf: list[str] = Query(["__default__"]),
                 sfop: SolrOperator = SolrOperator.SolrOr,
                 date: list[str] = Query([]), fq: list[str] = Query([]),
                 fqop: SolrOperator = SolrOperator.SolrAnd,
                 maxrows: int = 1000, start: int = 0):
    return (
        specification,
        QueryOptions(ctxt,
                     [field if field != "__default__" else specification.default_field for field in sf],
                     sfop,
                     fq, fqop,
                     specification.date_field,
                     [d.replace(";", " TO ") for d in date],
                     maxrows, start)
    )


def split_matches(window_size: int, highlights: list[str]) -> Iterable[tuple[str, str, str]]:
    for highlight in highlights:
        for match in re.finditer(r"<match>(.*?)</match>", highlight):
            hl = match.group(1)
            pre = re.sub(r"</?match>", "", highlight[:match.start(0)])[-window_size:]
            post = re.sub(r"</?match>", "", highlight[match.end(0):])[:window_size]
            yield (pre, hl, post)


def count_query(specification: CollectionSpecification = Depends(get_corpus),
                ctxt: Optional[int] = None,
                sf: list[str] = Query(["__default__"]),
                sfop: SolrOperator = SolrOperator.SolrOr,
                date: list[str] = Query([]), fq: list[str] = Query([]),
                fqop: SolrOperator = SolrOperator.SolrAnd):
    return (
        specification,
        QueryOptions(ctxt,
                     [field if field != "__default__" else specification.default_field for field in sf],
                     sfop,
                     fq, fqop,
                     specification.date_field,
                     [d.replace(";", " TO ") for d in date],
                     0, 0)
    )


@router.get("/{collection}/rsearch_count/{query}")
async def count_number_documents(query: str,
                                 params: tuple[CollectionSpecification, QueryOptions] = Depends(count_query),
                                 solr_server: SolrServer = Depends(get_solr_server)):
    specification, options = params
    n_docs = await solr_server.count_number_documents(specification.collection, query, options)
    return n_docs


@router.get("/{collection}/rsearch/{query}")
async def search_collection(query: str,
                            params: tuple[CollectionSpecification, QueryOptions] = Depends(search_query),
                            solr_server: SolrServer = Depends(get_solr_server)):
    specification, options = params
    docs, highlights = await solr_server.search_collection(specification.collection, query, options)

    results = []
    for doc in docs:
        basedoc = specification.as_basedoc(doc)
        if highlights is not None:
            for field, hl_matchs in highlights[basedoc.doc_id].items():
                for pre, hl, post in split_matches(options.ctxt // 2, hl_matchs):
                    result = asdict(basedoc)
                    del result["text"]
                    result.update({"before": pre, "matched": hl, "after": post, "field": field})
                    results.append(result)
        else:
            result = asdict(basedoc)
            del result["text"]
            results.append(result)

    root = et.Element("Results")
    for result in results:
        match_xml = et.SubElement(root, "match")
        for key, value in result.items():
            sub_xml = et.SubElement(match_xml, to_camel_case(key))
            sub_xml.text = clean_string(value)
    xml_str = et.tostring(root, encoding="unicode", method="xml")

    return Response(content=xml_str, media_type="application/xml")


@router.get("/{collection}/getdoc/{docid}")
async def get_document(docid: str,
                       specification: CollectionSpecification = Depends(get_corpus),
                       solr_server: SolrServer = Depends(get_solr_server)):
    doc = await solr_server.get_document(specification.collection, docid)
    basedoc = specification.as_basedoc(doc) if doc is not None else None

    root = et.Element("Results")
    if basedoc is not None:
        match_xml = et.SubElement(root, "match")
        for key, value in asdict(basedoc).items():
            sub_xml = et.SubElement(match_xml, to_camel_case(key))
            sub_xml.text = clean_string(value)
    xml_str = et.tostring(root, encoding="unicode", method="xml")

    return Response(content=xml_str, media_type="application/xml")


def convert2csv(rows: list[dict], fieldnames: list[str], convert_to_camel_case: bool = False):
    strbuf = StringIO()
    writer = csv.DictWriter(strbuf, fieldnames)
    writer.writeheader()
    for row in rows:
        if convert_to_camel_case:
            row = {to_camel_case(key): value for key, value in row.items()}
        writer.writerow(row)
    value = strbuf.getvalue()
    strbuf.close()
    return value


@router.post("/{collection}/getdocuments")
async def get_documents(request: Request,
                        specification: CollectionSpecification = Depends(get_corpus),
                        solr_server: SolrServer = Depends(get_solr_server)):
    body = (await request.body()).decode('utf-8')
    docids = filter(lambda x: len(x.strip()) > 0, body.split('\n'))
    docs = []
    for docid in docids:
        doc = await solr_server.get_document(specification.collection, docid)
        if doc is None:
            continue
        basedoc = specification.as_basedoc(doc)
        docs.append(asdict(basedoc))

    csv_docs = convert2csv(docs, ["DocId", "Date", "Title", "Source", "Text"], True)
    return Response(content=csv_docs, media_type="text/csv")


@router.post("/{collection}/getdocuments/{fields}")
async def get_field_content(fields: str,
                            request: Request,
                            specification: CollectionSpecification = Depends(get_corpus),
                            solr_server: SolrServer = Depends(get_solr_server)):
    fields = fields.split('#')
    body = (await request.body()).decode('utf-8')
    docids = filter(lambda x: len(x.strip()) > 0, body.split('\n'))
    docs = []
    for docid in docids:
        doc = await solr_server.get_document(specification.collection, docid)
        if doc is None:
            continue
        fielddoc = {field: doc.get(field, "") for field in fields}
        fielddoc["DocId"] = doc["id"]
        docs.append(fielddoc)

    csv_docs = convert2csv(docs, ["DocId"] + fields)
    return Response(content=csv_docs, media_type="text/csv")


@router.get("/{collection}/countdocs/{query}")
async def count_documents(query: str, by_field: str = "date", sf: list[str] = Query(["__default__"]),
                          sfop: SolrOperator = SolrOperator.SolrOr,
                          date_start: Optional[str] = None, date_end: Optional[str] = None,
                          date_gap_type: SolrRangeGapType = SolrRangeGapType.SolrYear,
                          date_gap: int = 1,
                          specification: CollectionSpecification = Depends(get_corpus),
                          solr_server: SolrServer = Depends(get_solr_server)):
    sf = [field if field != "__default__" else specification.default_field for field in sf]
    if by_field == "date" and specification.date_field is None:
        return {"status": "INVALID FIELD",
                "message": f"{specification.collection} does not support dates."}
    if by_field != "date" and by_field not in specification.filter_fields:
        return {"status": "INVALID FIELD",
                "message": f"{by_field} is not a field in {specification.collection}."}

    if by_field == "date" and date_start is not None and date_end is not None:
        counts = await solr_server.count_dates(specification.collection,
                                               date_start, date_end,
                                               date_gap_type, date_gap,
                                               query, sf, sfop)
    else:
        counts = await solr_server.count_docs(specification.collection, query, sf, sfop, by_field)

    root = et.Element("Results")
    for value, count in counts.items():
        et.SubElement(root, "Count", **{by_field: value, "n": str(count)})
    xml_str = et.tostring(root, encoding="unicode", method="xml")

    return Response(content=xml_str, media_type="application/xml")


# DEPRECATED ROUTES
def deprecated() -> str:
    return {
        "status": "DEPRECATED ROUTE",
        "message": "Your HistText client is using deprecated functions. Please update to a newer version."
    }


# /simple-search/:query
@router.get("/{collection}/simple-search/{query}")
def deprecated_simple_search(collection: str, query: str):
    return deprecated()


# /rsearch/:ctxt/:query
# /rsearch/:ctxt/:start/:searchfields/:query
# /rsearch/:ctxt/:start/:searchfields/:dates/:query
@router.get("/{collection}/rsearch/{ctxt}/{rest_of_path:path}")
def deprecated_search(collection: str, ctxt: str, rest_of_path: str):
    return deprecated()
