import csv
import logging
from io import StringIO
from typing import Optional

from fastapi import APIRouter, Depends, Request, Response, HTTPException

from ..annotation import (ModelStatus, apply_model, apply_model_on_solr,
                          supported_models)
from ..dependencies import get_corpus, get_solr_server
from ..solr import SolrServer
from ..specifications import CollectionSpecification
from ..models.precomputed import get_collections_with_precomputed_support, get_fields_with_precomputed_support

logger = logging.getLogger(__name__)
router = APIRouter()


def model_status_to_exception(status: ModelStatus):
    if status == ModelStatus.ModelNotImplemented:
        raise HTTPException(500, "Requested NER model is not correctly implemented on the server.")
    if status == ModelStatus.TooManyUsers:
        raise HTTPException(503, "Too many concurrent users. Please try again later.")
    if status == ModelStatus.OtherModelInUse:
        raise HTTPException(503, "Other model currently in use by other users. Please try again later.")
    if status == ModelStatus.UnknownModel:
        raise HTTPException(404, "Requested NER model is unknown to the server.")


@router.get("/{collection}-ner/list_precomputed_fields")
async def list_precomputed_fields(specification: Optional[CollectionSpecification] = Depends(get_corpus)):
    return list(get_fields_with_precomputed_support("ner", specification.collection))


@router.post("/{collection}-ner/extract")
async def ner_on_corpus(request: Request,
                        model: str = "__default__", field: str = "__default__",
                        precomputed: bool = False,
                        specification: Optional[CollectionSpecification] = Depends(get_corpus),
                        solr_server: SolrServer = Depends(get_solr_server)):
    body = (await request.body()).decode('utf-8')
    docids = filter(lambda x: len(x.strip()) > 0, body.split('\n'))

    if model == "__default__":
        model = specification.default_ner
    if not model.endswith(":ner"):
        raise HTTPException(404, detail="Invalid model name for NER")

    if field == "__default__":
        field = specification.default_field

    str_buf = StringIO()
    writer = csv.writer(str_buf)
    writer.writerow(["DocId", "Type", "Text", "Start", "End", "Confidence"])
    for docid in docids:
        result = await apply_model_on_solr(solr_server, specification.collection,
                                           field, model, docid, precomputed)
        if result is None:
            continue
        if result["status"] != ModelStatus.OK:
            str_buf.close()
            model_status_to_exception(result["status"])
            return {"status": result["status"]}
        for entity in result["result"]:
            writer.writerow([docid, entity["labels"][0], entity["text"],
                             entity["start_pos"], entity["end_pos"],
                             entity.get("confidence", -1.0)])
    content = str_buf.getvalue()
    str_buf.close()
    return Response(content=content, media_type="text/csv")


@router.get("/ner/list_models")
def list_models():
    return [display_name for display_name, path_name in supported_models.items() if path_name is not None and display_name.endswith(":ner")]


@router.get("/ner/list_precomputed_collections")
def list_precomputed_collections():
    return list(get_collections_with_precomputed_support("ner"))


@router.get("/ner/get_default_model/{collection}")
def get_default_ner_model(specification: Optional[CollectionSpecification] = Depends(get_corpus)):
    return {"model": specification.default_ner}


def get_model_name(model: str) -> str:
    """Compatibility dependency function to resolve model names."""
    if model == "eng":
        return "spacy:en:ner"
    if model == "mdn":
        return "spacy:zh:ner"
    return model


@router.post("/ner/{model}/extract")
async def ner_on_custom(request: Request,
                        model: str = Depends(get_model_name)):
    if not model.endswith(":ner"):
        raise HTTPException(404, detail="Invalid model name for NER")
    body = (await request.body()).decode('utf-8')
    lines = filter(lambda x: len(x.strip()) > 0, body.split('\n'))

    str_buf = StringIO()
    writer = csv.writer(str_buf)
    writer.writerow(["Type", "Text", "Start", "End", "Confidence"])
    for line in lines:
        result = await apply_model(model, line)
        if result["status"] != ModelStatus.OK:
            str_buf.close()
            model_status_to_exception(result["status"])
            return {"status": result["status"]}
        for entity in result["result"]:
            start = entity["start_pos"]
            end = entity["end_pos"]
            writer.writerow([entity["labels"][0], entity["text"],
                             start, end, entity.get("confidence", -1.0)])
    content = str_buf.getvalue()
    str_buf.close()
    return Response(content=content, media_type="text/csv")
