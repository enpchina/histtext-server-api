import csv
import logging
from io import StringIO
from typing import Optional
import regex as re

from fastapi import APIRouter, Depends, Request, Response, HTTPException

from ..annotation import (ModelStatus, apply_model, apply_model_on_solr,
                          supported_models)
from ..dependencies import get_corpus, get_solr_server
from ..solr import SolrServer
from ..specifications import CollectionSpecification, Language
from ..models.precomputed import get_collections_with_precomputed_support, get_fields_with_precomputed_support

logger = logging.getLogger(__name__)
router = APIRouter()


def model_status_to_exception(status: ModelStatus):
    if status == ModelStatus.ModelNotImplemented:
        raise HTTPException(500, "Requested CWS model is not correctly implemented on the server.")
    if status == ModelStatus.TooManyUsers:
        raise HTTPException(503, "Too many concurrent users. Please try again later.")
    if status == ModelStatus.OtherModelInUse:
        raise HTTPException(503, "Other model currently in use by other users. Please try again later.")
    if status == ModelStatus.UnknownModel:
        raise HTTPException(404, "Requested CWS model is unknown to the server.")


@router.get("/{collection}-cws/list_precomputed_fields")
async def list_precomputed_fields(specification: Optional[CollectionSpecification] = Depends(get_corpus)):
    return list(get_fields_with_precomputed_support("cws", specification.collection))


@router.post("/{collection}-cws/apply")
async def cws_on_corpus(request: Request,
                        model: str = "__default__", field: str = "__default__",
                        detailed: bool = False,
                        separator: str = " ",
                        precomputed: bool = False,
                        specification: Optional[CollectionSpecification] = Depends(get_corpus),
                        solr_server: SolrServer = Depends(get_solr_server)):
    if specification.language != Language.Chinese:
        raise HTTPException(400, "CWS only relevant on Chinese texts.")

    body = (await request.body()).decode('utf-8')
    docids = filter(lambda x: len(x.strip()) > 0, body.split('\n'))

    if model == "__default__":
        model = specification.default_cws
        if model is None:
            raise HTTPException(400, f"Collection '{specification.collection}' does not have a default CWS model.")
    if not model.endswith(":cws"):
        raise HTTPException(404, "Invalid model name for CWS")

    if field == "__default__":
        field = specification.default_field

    str_buf = StringIO()
    writer = csv.writer(str_buf)
    if detailed:
        writer.writerow(["DocId", "Token", "Start", "End", "Confidence"])
    else:
        writer.writerow(["DocId", "Text"])
    for docid in docids:
        result = await apply_model_on_solr(solr_server, specification.collection,
                                           field, model, docid, precomputed)
        if result is None:
            continue
        if result["status"] != ModelStatus.OK:
            str_buf.close()
            model_status_to_exception(result["status"])
            return {"status": result["status"]}
        if detailed:
            for token in result["result"]:
                writer.writerow([docid, token["token"],
                                 token["start"], token["end"],
                                 token.get("score", -1.0)])
        else:
            writer.writerow([docid, re.sub(f"{separator}+", separator,
                                           separator.join(token["token"] for token in result["result"]))])
    content = str_buf.getvalue()
    str_buf.close()
    return Response(content=content, media_type="text/csv")


@router.get("/cws/list_models")
def list_models():
    return [display_name for display_name, path_name in supported_models.items() if path_name is not None and display_name.endswith(":cws")]


@router.get("/cws/list_precomputed_collections")
def list_precomputed_collections():
    return list(get_collections_with_precomputed_support("cws"))


@router.get("/cws/get_default_model/{collection}")
def get_default_cws_model(specification: Optional[CollectionSpecification] = Depends(get_corpus)):
    if specification.language != Language.Chinese:
        raise HTTPException(404, "CWS only relevant on Chinese texts.")
    return {"model": specification.default_cws}


@router.post("/cws/{model}/apply")
async def cws_on_custom(request: Request,
                        model: str,
                        detailed: bool = False,
                        separator: str = " "):
    if not model.endswith(":cws"):
        raise HTTPException(404, detail="Invalid model name for CWS")
    body = (await request.body()).decode('utf-8')
    lines = filter(lambda x: len(x.strip()) > 0, body.split('\n'))

    str_buf = StringIO()
    writer = csv.writer(str_buf)
    if detailed:
        writer.writerow(["Token", "Start", "End", "Confidence"])
    else:
        writer.writerow(["Text"])
    for line in lines:
        result = await apply_model(model, line)
        if result["status"] != ModelStatus.OK:
            str_buf.close()
            model_status_to_exception(result["status"])
            return {"status": result["status"]}

        if detailed:
            for token in result["result"]:
                start = token["start"]
                end = token["end"]
                writer.writerow([token["token"],
                                 start, end, token.get("confidence", -1.0)])
        else:
            writer.writerow([re.sub(f"{separator}+", separator,
                                    separator.join(token["token"] for token in result["result"]))])
    content = str_buf.getvalue()
    str_buf.close()
    return Response(content=content, media_type="text/csv")
