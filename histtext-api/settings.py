import logging

from pydantic import BaseSettings

from .solr import SolrServer

logging.config.fileConfig('logging.conf', disable_existing_loggers=False)

logger = logging.getLogger(__name__)


class Settings(BaseSettings):
    solr_host: str = "localhost"
    solr_port: int = 8983
    specifications_conf_yml: str = "specifications.yml"
    model_change_delay: int = 30
    model_max_concurrent_users: int = 1
    models_conf_yml: str = "models.yml"
    models_dir: str = "models"
    precomputed_annotations_dir: str = "annotations"

    class Config:
        env_file = ".env"


settings = Settings()
logger.info("--- HistText Server API settings ---")
logger.info("SOLR_HOST=%s", settings.solr_host)
logger.info("SOLR_PORT=%d", settings.solr_port)
logger.info("SPECIFICATIONS_CONF_YML=%s", settings.specifications_conf_yml)
logger.info("MODEL_CHANGE_DELAY=%d", settings.model_change_delay)
logger.info("MODEL_MAX_CONCURRENT_USERS=%d", settings.model_max_concurrent_users)
logger.info("MODELS_DIR=%s", settings.models_dir)
logger.info("MODELS_CONF_YML=%s", settings.models_conf_yml)
logger.info("PRECOMPUTED_ANNOTATIONS_DIR=%s", settings.precomputed_annotations_dir)
logger.info("------------------------------------")


solr_server = SolrServer(settings.solr_host, settings.solr_port)
