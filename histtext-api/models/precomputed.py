import json
import logging
import os
from contextlib import contextmanager
import gzip
from typing import Optional

import jsonlines

from ..settings import settings

logger = logging.getLogger(__name__)


@contextmanager
def jsonl_with_gzip_support(filepath: str, mode: str = 'r'):
    if filepath.endswith('.gz'):
        fh = gzip.open(filepath, mode)
    else:
        fh = open(filepath, mode)

    try:
        if mode in ["r"]:
            yield jsonlines.Reader(fh)
        elif mode in ["w", "a"]:
            yield jsonlines.Writer(fh)
        else:
            raise ValueError(f"invalid mode: {mode}")
    finally:
        fh.close()


def load_indexes() -> dict[tuple[str, str, str], dict[str, tuple[str, int]]]:
    indexes = {}
    annot_path = os.path.abspath(os.path.expanduser(os.path.expandvars(settings.precomputed_annotations_dir)))
    if not os.path.exists(annot_path):
        logger.warning("Precomputed annotations directory path does not exist: %s", annot_path)
        return indexes
    for model_entry in os.scandir(annot_path):
        if not model_entry.is_dir():
            continue
        for collection_entry in os.scandir(model_entry.path):
            if not collection_entry.is_dir():
                continue
            for field_entry in os.scandir(collection_entry.path):
                if not field_entry.is_dir():
                    continue
                try:
                    with open(field_entry.path + "/index.json", 'r') as fin:
                        indexes[(model_entry.name, collection_entry.name, field_entry.name)] = json.load(fin)
                except FileNotFoundError:
                    logger.debug("\"%s - %s - %s\" doesn't have an index.",
                                 model_entry.name, collection_entry.name, field_entry.name)
                    continue
                logger.info("Loaded \"%s - %s - %s\" cache index (%d entries).",
                            model_entry.name, collection_entry.name, field_entry.name,
                            len(indexes[(model_entry.name, collection_entry.name, field_entry.name)]))
    logger.info("Loaded %d annotation cache indexes.", len(indexes))
    return indexes


indexes = load_indexes()


def get_collections_with_precomputed_support(task: str) -> set[str]:
    return {c for m, c, _ in indexes.keys() if m.split(':')[-1] == task}


def get_fields_with_precomputed_support(task: str, collection: str) -> set[str]:
    return {f for m, c, f in indexes.keys() if collection == c and m.split(':')[-1] == task}


short2keys = {
    "ner": {
        "t": "text",
        "l": "labels",
        "s": "start_pos",
        "e": "end_pos",
        "c": "confidence"
    }
}
short2labels = {
    "ner": {
        "P": "PERSON",
        "N": "NORP",
        "F": "FAC",
        "O": "ORG",
        "G": "GPE",
        "L": "LOC",
        "PR": "PRODUCT",
        "E": "EVENT",
        "W": "WORK_OF_ART",
        "LA": "LAW",
        "D": "DATE",
        "T": "TIME",
        "PE": "PERCENT",
        "M": "MONEY",
        "Q": "QUANTITY",
        "OR": "ORDINAL",
        "C": "CARDINAL",
        "LG": "LANGUAGE",
        "MI": "MISC"
    }
}


def unshorten(annotations: list[dict], task: str) -> list[dict]:
    unshorts = []
    for annotation in annotations:
        unshort = {}
        for key, value in annotation.items():
            if key == "l":
                if isinstance(value, list):
                    value = list(map(lambda x: short2labels[task][x], value))
                else:
                    value = short2labels[task][value]
            unshort[short2keys[task][key]] = value
        unshorts.append(unshort)
    return unshorts


def get_precomputed_annotation(model_name: str, collection: str, field: str, docid: str) -> Optional[list[dict]]:
    def find_annotation(jsonl_in: jsonlines.Reader, position: int) -> list[dict]:
        for idx, annotation_json in enumerate(jsonl_in):
            if idx == position:
                if docid != annotation_json["id"]:
                    raise RuntimeError(f"Corrupted precomputed annotation cache ({model_name} - {collection} - {field} - {docid} - {position})")
                return annotation_json["annotation"] if "annotation" in annotation_json else unshorten(annotation_json["a"], model_name.split(':')[-1])
        return None

    if (model_name, collection, field) not in indexes or docid not in indexes[(model_name, collection, field)]:
        return None
    cache_idx, position = indexes[(model_name, collection, field)][docid]
    if os.path.exists(f"{settings.precomputed_annotations_dir}/{model_name}/{collection}/{field}/{cache_idx}.jsonl.gz"):
        cache_path = f"{settings.precomputed_annotations_dir}/{model_name}/{collection}/{field}/{cache_idx}.jsonl.gz"
    else:
        cache_path = f"{settings.precomputed_annotations_dir}/{model_name}/{collection}/{field}/{cache_idx}.jsonl"
    with jsonl_with_gzip_support(cache_path, 'r') as jsonl_in:
        return find_annotation(jsonl_in, position)
