import warnings
from collections import defaultdict
from typing import Any, Optional, Union

import numpy as np
import torch
from transformers import (AutoModelForTokenClassification, AutoTokenizer,
                          AutoConfig,
                          PreTrainedModel, PreTrainedTokenizer)
from transformers.pipelines import AggregationStrategy, QuestionAnsweringPipeline, pipeline
import regex as re

from .info import ModelInfo
from .num2chinese import num2chinese


class TransformerNER:
    model: PreTrainedModel
    tokenizer: PreTrainedTokenizer
    maxlen: Optional[int]
    stride: int
    aggregation_strategy: AggregationStrategy
    _device: bool = "cuda:0" if torch.cuda.is_available() else "cpu"

    def __init__(self, model_info: ModelInfo, stride: int):
        if model_info.model_max_length is None:
            self.tokenizer = AutoTokenizer.from_pretrained(model_info.path)
        else:
            self.tokenizer = AutoTokenizer.from_pretrained(model_info.path,
                                                           model_max_length=model_info.model_max_length)
        self.model = AutoModelForTokenClassification.from_pretrained(model_info.path).to(self._device)
        self.model.eval()
        self.maxlen = model_info.model_max_length
        self.stride = stride
        self.aggregation_strategy = model_info.aggregation_strategy
        self.framework = 'pt'

    def _encode(self, text: str):
        model_inputs = self.tokenizer(text, truncation=True, max_length=self.maxlen, stride=self.stride,
                                      return_attention_mask=False,
                                      return_overflowing_tokens=True,
                                      return_special_tokens_mask=True,
                                      return_offsets_mapping=True,
                                      return_tensors='np')

        num_spans = len(model_inputs["input_ids"])

        for span_idx in range(num_spans):
            span_model_inputs = {}
            for k, v in model_inputs.items():
                span_v = v[span_idx]
                if k in self.tokenizer.model_input_names:
                    tensor = torch.tensor(span_v)
                    if tensor.dtype == torch.int32:
                        tensor = tensor.long()
                    span_model_inputs[k] = tensor.unsqueeze(0).to(self._device)
                elif k == "offset_mapping":
                    ndarray = np.array(span_v)
                    span_model_inputs[k] = ndarray  # np.expand_dims(ndarray, axis=0)
                else:
                    span_model_inputs[k] = span_v

            yield span_model_inputs

    def __call__(self, text: str) -> list[dict[str, Any]]:
        model_outputs = defaultdict(list)
        for span_inputs in self._encode(text):
            model_inputs = {k: span_inputs[k] for k in self.tokenizer.model_input_names if k in span_inputs}
            with torch.no_grad():
                span_logits = self.model(**model_inputs)[0]
            model_outputs["logits"].append(span_logits)
            for k, v in span_inputs.items():
                model_outputs[k].append(v)

        return self._decode(text, model_outputs)

    def _decode(self, text: str, model_outputs: dict):
        num_spans = len(model_outputs["input_ids"])
        logits = None
        input_ids = None
        special_tokens_mask = None
        for span_idx in range(num_spans):
            span_logits = model_outputs["logits"][span_idx][0].cpu().numpy()
            span_input_ids = model_outputs["input_ids"][span_idx][0].cpu().numpy()
            span_special_tokens_mask = model_outputs["special_tokens_mask"][span_idx]
            span_offset_mapping = model_outputs["offset_mapping"][span_idx]
            if logits is None:
                logits = span_logits
                input_ids = span_input_ids
                special_tokens_mask = span_special_tokens_mask
                offset_mapping = span_offset_mapping
            else:
                start_offset = 1 if span_special_tokens_mask[-1] == 1 else 0
                end_offset = -1 if span_special_tokens_mask[0] == 1 else None
                logits = np.concatenate((logits[:end_offset], span_logits[self.stride + start_offset:]))
                input_ids = np.concatenate((input_ids[:end_offset],
                                            span_input_ids[self.stride + start_offset:]))
                special_tokens_mask = np.concatenate((special_tokens_mask[:end_offset],
                                                      span_special_tokens_mask[self.stride + start_offset:]))
                offset_mapping = np.concatenate((offset_mapping[:end_offset],
                                                 span_offset_mapping[self.stride + start_offset:]))

        maxes = np.max(logits, axis=-1, keepdims=True)
        shifted_exp = np.exp(logits - maxes)
        scores = shifted_exp / shifted_exp.sum(axis=-1, keepdims=True)

        pre_entities = self._gather_pre_entities(
            text, input_ids, scores, offset_mapping, special_tokens_mask, self.aggregation_strategy
        )

        grouped_entities = self._aggregate(pre_entities, self.aggregation_strategy, text)

        entities = [
            entity
            for entity in grouped_entities
            if entity.get("entity", None) not in ["O"] and entity.get("entity_group", None) not in ["O"]
        ]
        return entities

    def _gather_pre_entities(
        self,
        sentence: str,
        input_ids: np.ndarray,
        scores: np.ndarray,
        offset_mapping: Optional[list[tuple[int, int]]],
        special_tokens_mask: np.ndarray,
        aggregation_strategy: AggregationStrategy,
    ) -> list[dict]:
        """Fuse various numpy arrays into dicts with all the information needed for aggregation."""
        pre_entities = []
        for idx, token_scores in enumerate(scores):
            # Filter special_tokens, they should only occur
            # at the sentence boundaries since we're not encoding pairs of
            # sentences so we don't have to keep track of those.
            if special_tokens_mask[idx]:
                continue

            word = self.tokenizer.convert_ids_to_tokens(int(input_ids[idx]))
            if offset_mapping is not None:
                start_ind = offset_mapping[idx][0]
                end_ind = offset_mapping[idx][1]
                if start_ind == end_ind:
                    continue
                word_ref = sentence[start_ind:end_ind]
                if getattr(self.tokenizer._tokenizer.model, "continuing_subword_prefix", None):
                    # This is a BPE, word aware tokenizer, there is a correct way
                    # to fuse tokens
                    is_subword = len(word) != len(word_ref)
                else:
                    # This is a fallback heuristic. This will fail most likely on any kind of text + punctuation mixtures that will be considered "words". Non word aware models cannot do better than this unfortunately.
                    if aggregation_strategy in {
                        AggregationStrategy.FIRST,
                        AggregationStrategy.AVERAGE,
                        AggregationStrategy.MAX,
                    }:
                        warnings.warn("Tokenizer does not support real words, using fallback heuristic", UserWarning)
                    is_subword = sentence[start_ind - 1:start_ind] != " " if start_ind > 0 else False

                if int(input_ids[idx]) == self.tokenizer.unk_token_id:
                    word = word_ref
                    is_subword = False
            else:
                start_ind = None
                end_ind = None
                is_subword = False

            pre_entity = {
                "word": word,
                "scores": token_scores,
                "start": start_ind,
                "end": end_ind,
                "index": idx,
                "is_subword": is_subword,
            }
            pre_entities.append(pre_entity)
        return pre_entities

    def _aggregate(self, pre_entities: list[dict], aggregation_strategy: AggregationStrategy, text: str) -> list[dict]:
        if aggregation_strategy in {AggregationStrategy.NONE, AggregationStrategy.SIMPLE}:
            entities = []
            for pre_entity in pre_entities:
                entity_idx = pre_entity["scores"].argmax()
                score = pre_entity["scores"][entity_idx]
                entity = {
                    "entity": self.model.config.id2label[entity_idx],
                    "score": score,
                    "index": pre_entity["index"],
                    "word": pre_entity["word"],
                    "start": pre_entity["start"],
                    "end": pre_entity["end"],
                }
                entities.append(entity)
        else:
            entities = self._aggregate_words(pre_entities, aggregation_strategy)

        if aggregation_strategy == AggregationStrategy.NONE:
            return entities
        return self._group_entities(entities, text)

    def _aggregate_word(self, entities: list[dict], aggregation_strategy: AggregationStrategy) -> dict:
        word = self.tokenizer.convert_tokens_to_string([entity["word"] for entity in entities])
        if aggregation_strategy == AggregationStrategy.FIRST:
            scores = entities[0]["scores"]
            idx = scores.argmax()
            score = scores[idx]
            entity = self.model.config.id2label[idx]
        elif aggregation_strategy == AggregationStrategy.MAX:
            max_entity = max(entities, key=lambda entity: entity["scores"].max())
            scores = max_entity["scores"]
            idx = scores.argmax()
            score = scores[idx]
            entity = self.model.config.id2label[idx]
        elif aggregation_strategy == AggregationStrategy.AVERAGE:
            scores = np.stack([entity["scores"] for entity in entities])
            average_scores = np.nanmean(scores, axis=0)
            entity_idx = average_scores.argmax()
            entity = self.model.config.id2label[entity_idx]
            score = average_scores[entity_idx]
        else:
            raise ValueError("Invalid aggregation_strategy")
        new_entity = {
            "entity": entity,
            "score": score,
            "word": word,
            "start": entities[0]["start"],
            "end": entities[-1]["end"],
        }
        return new_entity

    def _aggregate_words(self, entities: list[dict], aggregation_strategy: AggregationStrategy) -> list[dict]:
        """
        Override tokens from a given word that disagree to force agreement on word boundaries.

        Example: micro|soft| com|pany| B-ENT I-NAME I-ENT I-ENT will be rewritten with first
        strategy as microsoft|company| B-ENT I-ENT
        """
        if aggregation_strategy in {
            AggregationStrategy.NONE,
            AggregationStrategy.SIMPLE,
        }:
            raise ValueError("NONE and SIMPLE strategies are invalid for word aggregation")

        if not entities:
            return []

        word_entities = []
        word_group = None
        for entity in entities:
            if word_group is None:
                word_group = [entity]
            elif entity["is_subword"]:
                word_group.append(entity)
            else:
                word_entities.append(self._aggregate_word(word_group, aggregation_strategy))
                word_group = [entity]
        # Last item
        word_entities.append(self._aggregate_word(word_group, aggregation_strategy))
        return word_entities

    def _group_sub_entities(self, entities: list[dict], text: str) -> dict:
        """
        Group together the adjacent tokens with the same entity predicted.

        Args:
            entities (`dict`): The entities predicted by the pipeline.
        """
        # Get the first entity in the entity group
        entity = entities[0]["entity"].split("-")[-1]
        scores = np.nanmean([entity["score"] for entity in entities])
        # tokens = [entity["word"] for entity in entities]

        entity_group = {
            "entity_group": entity,
            "score": np.mean(scores),
            # "word": self.tokenizer.convert_tokens_to_string(tokens),
            "word": text[entities[0]["start"]:entities[-1]["end"]],
            "start": entities[0]["start"],
            "end": entities[-1]["end"],
        }
        return entity_group

    def _get_tag(self, entity_name: str) -> tuple[str, str]:
        if entity_name.startswith("B-"):
            bi = "B"
            tag = entity_name[2:]
        elif entity_name.startswith("I-"):
            bi = "I"
            tag = entity_name[2:]
        else:
            # It's not in B-, I- format
            # Default to I- for continuation.
            bi = "I"
            tag = entity_name
        return bi, tag

    def _group_entities(self, entities: list[dict], text: str) -> list[dict]:
        """
        Find and group together the adjacent tokens with the same entity predicted.

        Args:
            entities (`dict`): The entities predicted by the pipeline.
        """
        entity_groups = []
        entity_group_disagg = []

        for entity in entities:
            if not entity_group_disagg:
                entity_group_disagg.append(entity)
                continue

            # If the current entity is similar and adjacent to the previous entity,
            # append it to the disaggregated entity group
            # The split is meant to account for the "B" and "I" prefixes
            # Shouldn't merge if both entities are B-type
            bi, tag = self._get_tag(entity["entity"])
            last_bi, last_tag = self._get_tag(entity_group_disagg[-1]["entity"])

            if tag == last_tag and bi != "B":
                # Modify subword type to be previous_type
                entity_group_disagg.append(entity)
            else:
                # If the current entity is different from the previous entity
                # aggregate the disaggregated entity group
                entity_groups.append(self._group_sub_entities(entity_group_disagg, text))
                entity_group_disagg = [entity]
        if entity_group_disagg:
            # it's the last entity, add it to the entity groups
            entity_groups.append(self._group_sub_entities(entity_group_disagg, text))

        return entity_groups


class TransformerCWS:
    model: PreTrainedModel
    tokenizer: PreTrainedTokenizer
    maxlen: Optional[int]
    stride: int
    _device: str = "cuda:0" if torch.cuda.is_available() else "cpu"

    def __init__(self, model_info: ModelInfo, stride: int):
        config = AutoConfig.from_pretrained(model_info.path)
        kwargs = {}
        if model_info.model_max_length is not None:
            kwargs["model_max_length"] = model_info.model_max_length
        if config.model_type in {"gpt2", "roberta"}:
            kwargs["add_prefix_space"] = True
        self.tokenizer = AutoTokenizer.from_pretrained(model_info.path, use_fast=True,
                                                       **kwargs)
        self.model = AutoModelForTokenClassification.from_pretrained(model_info.path).to(self._device)
        self.model.eval()
        self.maxlen = model_info.model_max_length
        self.stride = stride

    def _rebuild_offsets_mapping(self, offsets_mapping: torch.Tensor, stride: int,
                                 word_ids: list[list[Optional[int]]]):
        # last_end_idx = 0
        cur_word_id = 0
        for span_idx in range(len(offsets_mapping)):
            fword_ids: list[int] = list(filter(lambda x: x is not None, word_ids[span_idx]))  # type:ignore
            cur_word_id = 0
            for char_idx in range(len(offsets_mapping[span_idx])):
                if offsets_mapping[span_idx][char_idx][0] == offsets_mapping[span_idx][char_idx][1]:
                    continue

                offsets_mapping[span_idx][char_idx] = (fword_ids[cur_word_id], fword_ids[cur_word_id] + 1)
                cur_word_id += 1
                if cur_word_id >= len(fword_ids):
                    break

    def _encode(self, text: list[str]):
        model_inputs = self.tokenizer(text, truncation=True, max_length=self.maxlen, stride=self.stride,
                                      return_attention_mask=False,
                                      return_overflowing_tokens=True,
                                      return_special_tokens_mask=True,
                                      return_offsets_mapping=True,
                                      is_split_into_words=True,
                                      return_tensors='np')
        num_spans = len(model_inputs["input_ids"])
        word_ids = [model_inputs.word_ids(span_idx) for span_idx in range(num_spans)]
        self._rebuild_offsets_mapping(model_inputs["offset_mapping"], self.stride,
                                      word_ids)

        for span_idx in range(num_spans):
            span_model_inputs = {}
            for k, v in model_inputs.items():
                span_v = v[span_idx]
                if k in self.tokenizer.model_input_names:
                    tensor = torch.tensor(span_v)
                    if tensor.dtype == torch.int32:
                        tensor = tensor.long()
                    span_model_inputs[k] = tensor.unsqueeze(0).to(self._device)
                elif k == "offset_mapping":
                    ndarray = np.array(span_v)
                    span_model_inputs[k] = ndarray
                else:
                    span_model_inputs[k] = span_v

            yield span_model_inputs

    def __call__(self, text: str, aggregate: bool = True) -> list[dict[str, Any]]:
        # text = text.replace('　', '')
        model_outputs = defaultdict(list)
        for span_inputs in self._encode([char for char in text]):
            model_inputs = {k: span_inputs[k] for k in self.tokenizer.model_input_names if k in span_inputs}
            with torch.no_grad():
                span_logits = self.model(**model_inputs)[0]
            model_outputs["logits"].append(span_logits)
            for k, v in span_inputs.items():
                model_outputs[k].append(v)

        return self._decode(text, model_outputs, aggregate)

    def _decode(self, text: str, model_outputs: dict, aggregate: bool = True):
        num_spans = len(model_outputs["input_ids"])
        logits = None
        input_ids = None
        special_tokens_mask = None
        for span_idx in range(num_spans):
            span_logits = model_outputs["logits"][span_idx][0].cpu().numpy()
            span_input_ids = model_outputs["input_ids"][span_idx][0].cpu().numpy()
            span_special_tokens_mask = model_outputs["special_tokens_mask"][span_idx]
            span_offset_mapping = model_outputs["offset_mapping"][span_idx]
            if logits is None:
                logits = span_logits
                input_ids = span_input_ids
                special_tokens_mask = span_special_tokens_mask
                offset_mapping = span_offset_mapping
            else:
                start_offset = 1 if span_special_tokens_mask[-1] == 1 else 0
                end_offset = -1 if span_special_tokens_mask[0] == 1 else None
                logits = np.concatenate((logits[:end_offset], span_logits[self.stride + start_offset:]))
                input_ids = np.concatenate((input_ids[:end_offset],
                                            span_input_ids[self.stride + start_offset:]))
                special_tokens_mask = np.concatenate((special_tokens_mask[:end_offset],
                                                      span_special_tokens_mask[self.stride + start_offset:]))
                offset_mapping = np.concatenate((offset_mapping[:end_offset],
                                                 span_offset_mapping[self.stride + start_offset:]))

        maxes = np.max(logits, axis=-1, keepdims=True)
        shifted_exp = np.exp(logits - maxes)
        scores = shifted_exp / shifted_exp.sum(axis=-1, keepdims=True)

        pre_tokens = self._gather_pre_tokens(text, input_ids, scores, offset_mapping, special_tokens_mask)
        return self._aggregate(pre_tokens, text, aggregate)

    def _gather_pre_tokens(
        self,
        sentence: str,
        input_ids: np.ndarray,
        scores: np.ndarray,
        offset_mapping: Optional[list[tuple[int, int]]],
        special_tokens_mask: np.ndarray
    ) -> list[dict]:
        """Fuse various numpy arrays into dicts with all the information needed for aggregation."""
        pre_entities = []
        for idx, token_scores in enumerate(scores):
            # Filter special_tokens, they should only occur
            # at the sentence boundaries since we're not encoding pairs of
            # sentences so we don't have to keep track of those.
            if special_tokens_mask[idx]:
                continue

            word = self.tokenizer.convert_ids_to_tokens(int(input_ids[idx]))
            if offset_mapping is not None:
                start_ind = offset_mapping[idx][0]
                end_ind = offset_mapping[idx][1]
                if start_ind == end_ind:
                    continue
                word_ref = sentence[start_ind:end_ind]
                if int(input_ids[idx]) == self.tokenizer.unk_token_id:
                    word = word_ref
            else:
                start_ind = None
                end_ind = None

            pre_entity = {
                "word": word,
                "scores": token_scores,
                "start": start_ind,
                "end": end_ind,
                "index": idx,
            }
            pre_entities.append(pre_entity)
        return pre_entities

    def _aggregate(self, pre_entities: list[dict], text: str,
                   aggregate: bool = True) -> list[dict]:
        entities = []
        for pre_entity in pre_entities:
            entity_idx = pre_entity["scores"].argmax()
            score = pre_entity["scores"][entity_idx]
            entity = {
                "entity": self.model.config.id2label[entity_idx],
                "score": score,
                "index": pre_entity["index"],
                "word": pre_entity["word"],
                "start": pre_entity["start"],
                "end": pre_entity["end"],
            }
            entities.append(entity)

        return self._group_tokens(entities, text) if aggregate else entities

    def _group_sub_tokens(self, sub_tokens: list[dict], text: str) -> dict:
        score = np.nanmean([sub_token["score"] for sub_token in sub_tokens])

        token = {
            "score": score,
            "word": "".join(sub_token["word"] for sub_token in sub_tokens),
            "start": sub_tokens[0]["start"],
            "end": sub_tokens[-1]["end"]
        }
        return token

    def _group_tokens(self, tokens: list[dict], text: str) -> list[dict]:
        token_groups = []
        token_group_disagg: list[dict] = []
        for token in tokens:
            if not token_group_disagg:
                token_group_disagg.append(token)
                continue
            tag = token["entity"]
            last_tag = token_group_disagg[-1]["entity"]
            if last_tag in ["B", "I"] and tag in ["I"]:
                token_group_disagg.append(token)
            else:
                token_groups.append(self._group_sub_tokens(token_group_disagg, text))
                token_group_disagg = [token]
        if token_group_disagg:
            token_groups.append(self._group_sub_tokens(token_group_disagg, text))
        return token_groups


class TransformerQA:
    pipe: QuestionAnsweringPipeline
    _device: str = 0 if torch.cuda.is_available() else -1
    _support_impossible: bool = False

    def __init__(self, model_info: ModelInfo):
        self.pipe = pipeline("question-answering", model=model_info.path, device=self._device)
        if model_info.support_impossible is None:
            raise RuntimeError(f"No support impossible info given for QA model ({model_info.path}).")
        self._support_impossible = model_info.support_impossible

    def _get_topic_answers(self, topic: str,
                           questions: list[str], context: str,
                           max_answers: int, threshold: Optional[float],
                           biography: dict[str, list[tuple[str, int, float]]]):
        def prepare_repeat_question(question: str) -> list[str]:
            repeat_pattern = r"{#([\w\d]+:[\w\d]+)}"
            rep_variables: set[str] = set()
            for match in re.finditer(repeat_pattern, question):
                rep_variables.add(match.group(1))

            if not rep_variables:
                return [question]

            questions = []
            for variable in rep_variables:
                if isinstance(biography[variable], list):
                    values = biography[variable]
                else:
                    values = [biography[variable]]
                for value in values:
                    questions.append(question.replace(f"{{#{variable}}}", value["answer"]))
            return questions


        def prepare_count_questions(questions: list[str]) -> list[str]:
            count_pattern = r"{%(zh|la)}"
            count_questions = []
            for question in questions:
                if re.search(count_pattern, question) is None:
                    count_questions.append(question)
                    continue
                for count in range(1, max_answers + 1):
                    count_question = question.replace("{%en}", str(count))
                    count_question = count_question.replace("{%zh}", num2chinese(count))
                    count_questions.append(count_question)

            return count_questions


        def prepare_question(question: str) -> list[str]:
            variable_pattern = r"{([\w\d]+:[\w\d]+)}"

            variables: set[str] = set()
            for match in re.finditer(variable_pattern, question):
                variables.add(match.group(1))

            for variable in variables:
                if not biography[variable]:
                    return []
                value = biography[variable][0]
                question = question.replace(f"{{{variable}}}", value["answer"])

            questions = prepare_repeat_question(question)
            questions = prepare_count_questions(questions)

            return questions

        def answer_already_given(indices: list[tuple[int, int]],
                                 new_start: int, new_end: int) -> bool:
            for start, end in indices:
                if start <= new_start < end or start < new_end <= end or new_start <= start < end <= new_end:
                    return True
            return False

        def filter_answers(answers: list[dict[str, Union[float, int, str]]],
                           max_answers: int,
                           threshold: Optional[float]) -> list[dict[str, Union[float, int, str]]]:
            kept_answers: list[dict[str, Union[float, int, str]]] = []
            kept_indices: list[tuple[int, int]] = []

            for answer in answers:
                if len(kept_answers) >= max_answers:
                    break
                if answer["answer"] == '':
                    break
                if answer_already_given(kept_indices, answer["start"], answer["end"]):
                    continue
                if threshold is not None and answer["score"] < threshold:
                    continue
                kept_answers.append(answer)
                kept_indices.append((answer["start"], answer["end"]))

            return kept_answers

        topic_answers: dict[str, list[dict[str, Union[float, int, str]]]] = defaultdict(list)
        max_idx = -1
        for question in questions:
            subquestions = prepare_question(question)
            for idx, subquestion in enumerate(subquestions):
                answers = self.pipe(question=subquestion, context=context, top_k=50,
                                    handle_impossible_answer=self._support_impossible)
                answers = filter_answers(answers,
                                         max_answers=max_answers,
                                         threshold=threshold)
                key = f"{topic}:{idx}" if len(subquestions) > 1 else topic
                topic_answers[key].extend(answers)
                max_idx = idx
        for idx in range(max_idx+1):
            key = f"{topic}:{idx}" if max_idx > 0 else topic
            answers = sorted(topic_answers[key], key=lambda x: x["score"], reverse=True)
            answers = filter_answers(answers, max_answers, None)
            biography[key] = answers

    def __call__(self, context: str, questions: Union[list[str], dict[str, list[str]]],
                 max_answers: Union[int, dict[str, int]] = 1,
                 threshold: Union[Optional[float], dict[str, Optional[float]]] = None):
        biography: dict[str, list[tuple[str, int, float]]] = defaultdict(list)
        for topic in questions:
            max_ans = max_answers if isinstance(max_answers, int) else max_answers.get(topic, 1)
            thresh = threshold if isinstance(threshold, float) else threshold.get(topic, None)
            self._get_topic_answers(topic, questions[topic], context, max_ans, thresh, biography)

        return biography
