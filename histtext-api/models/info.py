from dataclasses import dataclass
from enum import Enum
from typing import Optional
from transformers.pipelines import AggregationStrategy


class ModelStatus(Enum):
    OK = "OK"
    Precomputed = "Precomputed"
    TooManyUsers = "Too Many Users"
    OtherModelInUse = "Other Model In Use"
    UnknownModel = "Unknown Model"
    ModelNotImplemented = "Model Not Implemented"
    SetupError = "Setup Error"
    UnknownError = "Unknown Error"
    WrongTask = "Wrong Task"


@dataclass
class ModelInfo:
    path: str
    model_max_length: Optional[int] = None
    aggregation_strategy: Optional[AggregationStrategy] = None
    support_impossible: Optional[bool] = None
