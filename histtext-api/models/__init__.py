from .info import ModelInfo, ModelStatus
from .trf_pipelines import TransformerNER, TransformerCWS, TransformerQA
# from .precomputed import get_precomputed_annotation
