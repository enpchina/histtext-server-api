"""Server API that allows interactions with SolR and other NLP tools.

The main goal of HistText Server API is to allow clients (such as the HistText R Client)
to interact with SolR collections and NLP models.
"""

__version_info__ = (0, 7, 1)
__version__ = ".".join(str(c) for c in __version_info__)
__author__ = "Jeremy Auguste"
__authoremail__ = "jeremy.auguste@univ-amu.fr"
