import asyncio
import logging
import threading
import time
from typing import Optional, Union
import os

import spacy
import yaml
from starlette.concurrency import run_in_threadpool
from transformers.pipelines import AggregationStrategy

from .models import ModelInfo, ModelStatus, TransformerNER, TransformerCWS, TransformerQA
from .models.precomputed import get_precomputed_annotation
from .settings import settings
from .solr import SolrServer

logger = logging.getLogger(__name__)


def load_supported_models():
    models = {}
    try:
        with open(settings.models_conf_yml, 'r') as fin:
            models_conf = yaml.safe_load(fin)
        for model in models_conf:
            if "path" in models_conf[model]:
                path_or_name = f"{settings.models_dir}/{models_conf[model]['path']}"
                if not os.path.exists(path_or_name):
                    logger.warning("Path for model '%s' does not exist: %s", model, path_or_name)
                    continue
            else:
                path_or_name = models_conf[model]["name"]
            strat = AggregationStrategy[models_conf[model].get("aggregation_strategy", "FIRST")]
            models[model] = ModelInfo(path_or_name,
                                      model_max_length=models_conf[model].get("max_length", None),
                                      aggregation_strategy=strat,
                                      support_impossible=models_conf[model].get("support_impossible", None))
    except FileNotFoundError:
        logger.warning("Models config file does not exist: %s", settings.models_conf_yml)
    logger.info("Registered %d models.", len(models))
    return models


supported_models = load_supported_models()


last_use = 0
current_model = None
current_model_name = None
model_in_use = 0
lock = threading.Lock()


async def setup_model(model_name: str) -> ModelStatus:
    global current_model, last_use, current_model_name, model_in_use

    if model_name not in supported_models:
        return ModelStatus.UnknownModel

    with lock:
        if model_in_use >= settings.model_max_concurrent_users:  # Limit concurrent users
            logger.warning("Exceeded concurrent users limit: %d/%d",
                           model_in_use, settings.model_max_concurrent_users)
            return ModelStatus.TooManyUsers
        logger.debug("Concurrent users: %d/%d", model_in_use, settings.model_max_concurrent_users)

        # Check that models are not changed too quickly/during usage
        diff = int(time.time()) - last_use
        if (current_model_name is not None
           and model_name != current_model_name
           and (model_in_use > 0 or diff < settings.model_change_delay)):
            logger.info("Denied request to other model '%s' (curr: '%s' - last use: %d - in use: %d/%d)",
                        model_name, current_model_name, diff,
                        model_in_use, settings.model_max_concurrent_users)
            return ModelStatus.OtherModelInUse

        model_in_use += 1

        if current_model_name == model_name:
            last_use = int(time.time())
            return ModelStatus.OK

        try:
            model_info = supported_models[model_name]
            if model_name.startswith("spacy"):
                current_model = None
                current_model_name = model_name
                current_model = spacy.load(model_info.path,
                                           exclude=["parser", "tagger",
                                                    "lemmatizer",
                                                    "attribute_ruler"])
                last_use = int(time.time())
                logger.info("Loaded model: %s", model_name)
                return ModelStatus.OK
            elif model_name.startswith("trftc"):
                current_model = None
                current_model_name = model_name
                if model_name.endswith("ner"):
                    current_model = TransformerNER(model_info, 10)
                elif model_name.endswith("cws"):
                    current_model = TransformerCWS(model_info, 10)
                elif model_name.endswith("qa"):
                    current_model = TransformerQA(model_info)
                else:
                    raise NotImplementedError(f"Model '{model_name}' is not implemented.")
                last_use = int(time.time())
                logger.info("Loaded model: %s", model_name)
                return ModelStatus.OK
            else:
                raise NotImplementedError(f"Model '{model_name}' is unknown.")
        except Exception as e:
            model_in_use = 0
            current_model = None
            current_model_name = None
            raise e

        model_in_use = 0
        current_model = None
        current_model_name = None
        return ModelStatus.ModelNotImplemented


async def spacy_ner(text: str):
    doc = await run_in_threadpool(current_model, text.replace("\n", " "))  # current_model(text)
    result = []
    for entity in doc.ents:
        result.append({
            "text": entity.text,
            "labels": [entity.label_],
            "start_pos": entity.start_char,
            "end_pos": entity.end_char
        })
    return {"status": ModelStatus.OK, "result": result}


async def transformers_pipeline_ner(text: str):
    ner_output = await run_in_threadpool(current_model, text.replace("\n", " "))
    result = []
    for entity in ner_output:
        result.append({
            "text": entity["word"],
            "labels": [entity["entity_group"]],
            "start_pos": entity["start"],
            "end_pos": entity["end"],
            "confidence": entity["score"]
        })
    return {"status": ModelStatus.OK, "result": result}


async def transformers_pipeline_cws(text: str):
    cws_output = await run_in_threadpool(current_model, text.replace("\n", " "))
    idx2token = {}
    for tok in cws_output:
        for char_idx in range(tok["start"], tok["end"]):
            idx2token[char_idx] = tok
    seen_idx = set()
    result = []
    for char_idx, char in enumerate(text):
        if char_idx in seen_idx:
            continue
        token: Optional[dict] = idx2token.get(char_idx, None)
        if token is None:
            result.append({
                "token": char,
                "start": char_idx,
                "end": char_idx + 1,
                "score": -1.0
            })
            seen_idx.add(char_idx)
            continue
        result.append({
            "token": text[token["start"]:token["end"]],
            "start": int(token["start"]),
            "end": int(token["end"]),
            "score": float(token["score"])
        })
        seen_idx.update(range(token["start"], token["end"]))
    return {"status": ModelStatus.OK, "result": result}


async def transformers_pipeline_qa(text: str, questions: Union[list[str], dict[str, list[str]]],
                                   max_answers: int = 1,
                                   threshold: float = 0.0):
    kwargs = {
        "context": text.replace("\n", " "),
        "questions": questions,
        "max_answers": max_answers,
        "threshold": threshold
    }
    qa_output = await run_in_threadpool(current_model, **kwargs)

    return {"status": ModelStatus.OK, "result": qa_output}


async def apply_model(model_name: str, text: str, other_info: Optional[dict] = None) -> dict:
    global model_in_use, last_use
    status = await setup_model(model_name)
    n_tries = 1
    while status == ModelStatus.TooManyUsers:
        await asyncio.sleep(10)
        status = await setup_model(model_name)
        n_tries += 1
        if n_tries > 5:
            return {"status": ModelStatus.TooManyUsers,
                    "message": "Too many concurrent users. Please try again later."}

    if status != ModelStatus.OK:
        return {"status": status}
    try:
        if current_model_name.startswith("spacy"):
            return await spacy_ner(text)
        if current_model_name.startswith("trftc"):
            if current_model_name.endswith("ner"):
                return await transformers_pipeline_ner(text)
            elif current_model_name.endswith("cws"):
                return await transformers_pipeline_cws(text)
            elif current_model_name.endswith("qa"):
                if other_info is None or "questions" not in other_info:
                    raise RuntimeError("Can't apply QA without a specified question.")
                return await transformers_pipeline_qa(text, **other_info)
            logger.error(f"Model '{current_model_name}' is not fully implemented.")
            return {"status": ModelStatus.ModelNotImplemented}
    except Exception as e:
        raise e
    finally:
        with lock:
            last_use = int(time.time())
            model_in_use -= 1
    return {"status": ModelStatus.UnknownModel}


async def apply_model_on_solr(solr_server: SolrServer, collection: str, field: str,
                              model_name: str, docid: str,
                              only_precomputed: bool = False,
                              other_info: Optional[dict] = None) -> Optional[dict]:
    if (annotation := get_precomputed_annotation(model_name, collection, field, docid)) is not None:
        return {"status": ModelStatus.OK, "result": annotation}
    if only_precomputed:
        return None
    doc = await solr_server.get_document(collection, docid)
    if doc is None:
        return None
    return await apply_model(model_name, doc[field], other_info)
