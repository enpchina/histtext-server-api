import logging
import regex as re
from dataclasses import dataclass
from enum import Enum
from typing import Optional

import yaml

from .settings import settings

logger = logging.getLogger(__name__)


@dataclass
class BaseDoc:
    doc_id: str
    source: str
    title: str
    text: str
    date: str


class Language(Enum):
    """Language enumeration used in specifications."""

    English = "en"
    Chinese = "zh"
    French = "fr"
    Multi = "multi"

    @staticmethod
    def from_str(value):
        value = value.lower()
        if value in ["en", "english", "eng"]:
            return Language.English
        if value in ["zh", "chinese", "mdn"]:
            return Language.Chinese
        if value in ["fr", "french"]:
            return Language.French
        if value in ["multi", "multilingual"]:
            return Language.Multi
        raise NotImplementedError(f"Unknown language: {value}")


class CollectionSpecification:
    """Generic collection specification class."""

    collection: str
    default_field: str
    search_fields: list[str]
    filter_fields: list[str]
    date_field: Optional[str]
    language: Language
    default_ner: str
    default_cws: Optional[str]
    basedoc_builder: Optional[dict]

    def __init__(self, collection: str, language: Language,
                 default_field: str, search_fields: list[str],
                 default_ner: str,
                 default_cws: Optional[str] = None,
                 filter_fields: list[str] = [],
                 date_field: Optional[str] = None,
                 basedoc_builder: Optional[dict] = None):
        self.collection = collection
        self.default_field = default_field
        self.search_fields = search_fields
        self.default_ner = default_ner
        self.default_cws = default_cws
        self.filter_fields = filter_fields
        self.date_field = date_field
        self.language = language
        self.basedoc_builder = basedoc_builder

    def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
        def get_basedoc_field(field):
            field_builder = self.basedoc_builder[field]
            if "static" in field_builder:
                return field_builder["static"]
            if "field" in field_builder:
                if "default" in field_builder:
                    return doc.get(field_builder["field"], field_builder["default"])
                else:
                    return doc[field_builder["field"]]
            if "oneliner" in field_builder:
                return eval(field_builder["oneliner"])
            raise RuntimeError(f"Missing field builder for '{field}'")
        if self.basedoc_builder is None:
            raise NotImplementedError()
        return BaseDoc(doc["id"],
                       get_basedoc_field("source"),
                       get_basedoc_field("title"),
                       get_basedoc_field("text"),
                       get_basedoc_field("date"))


def load_specification_config() -> dict[str, CollectionSpecification]:
    """Instantiate collection specifications from the YAML configuration file.

    Returns a dictionary with collection names as keys and specifications as values.
    """
    specifications = {}
    try:
        with open(settings.specifications_conf_yml, 'r') as fin:
            specs_conf = yaml.safe_load(fin)
    except FileNotFoundError:
        logger.warning("Collection specifications config file does not exist: %s",
                       settings.specifications_conf_yml)
        return specifications
    for spec_name in specs_conf:
        try:
            coll_spec = specs_conf[spec_name]["specification"]
            basedoc_builder = specs_conf[spec_name]["basedoc"]
            specification = CollectionSpecification(coll_spec["collection"],
                                                    Language.from_str(coll_spec["language"]),
                                                    coll_spec["default_field"],
                                                    coll_spec["search_fields"],
                                                    coll_spec["default_ner"],
                                                    coll_spec.get("default_cws", None),
                                                    coll_spec.get("filter_fields", []),
                                                    coll_spec.get("date_field", None),
                                                    basedoc_builder=basedoc_builder)
            specifications[spec_name] = specification
            logger.debug("Registered collection specification: %s", spec_name)
        except (RuntimeError, KeyError) as e:
            logger.warning("Error while loading configuration for specification: %s", spec_name)
            logger.warning(e)
    logger.info("Registered %d collection specifications.", len(specifications))
    return specifications


# class ImhCollectionSpecification(CollectionSpecification):
#     """ Specification for the IMH collections.

#     """
#     def __init__(self, language: Language):
#         super().__init__(
#             f"imh-{language.value}", language,
#             "story", ["story"],
#             f"spacy:zh:ner" if language == Language.Chinese else "trftc_noisy:en:ner",
#             ["book", "bookno", "page"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         story = doc["story"]
#         return BaseDoc(doc["id"], doc["book"],
#                        re.sub(r"<p[^/]+/p>", "", story).split("\n", 1)[0][:12].strip(),
#                        story,
#                        doc["date"])


# class WikibioCollectionSpecification(CollectionSpecification):
#     """ Specification for the wikibio collections.

#     """
#     def __init__(self, language: Language):
#         langid = "e" if language == Language.English else "z"
#         self._content_field = f"wk{langid}_content"
#         self._title_field = f"wk{langid}_title"
#         self._url_field = f"wk{langid}_url"
#         super().__init__(
#             f"wikibio-{language.value}", language,
#             self._content_field, [self._content_field, self._title_field],
#             f"spacy:{language.value}:ner",
#             [self._url_field, "wk_en_id", "wk_zh_id", "wk_img"],
#             None
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], "wikibio",
#                        doc[self._title_field],
#                        doc[self._content_field],
#                        "2020/06/01")


# class ReportsCollectionSpecification(CollectionSpecification):
#     """ Specification for the reports collections.

#     """
#     def __init__(self, language: Language):
#         super().__init__(
#             f"reports-{language.value}", language,
#             "text", ["text"],
#             # "spacy:en:ner",  # FIXME: Add a french model for the french reports
#             "trftc_noisy:en:ner",
#             ["source", "page"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], doc["source"],
#                        "",
#                        doc["text"],
#                        doc["date"])


# class ProquestCollectionSpecification(CollectionSpecification):
#     """ Specification for the Proquest collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "proquest", Language.English,
#             "fulltext", ["fulltext", "title"],
#             "trftc_noisy_nopunct:en:ner",
#             ["publisher", "category"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         fulltext = doc.get("fulltext", "")
#         return BaseDoc(doc["id"], doc["publisher"],
#                        doc.get("title", ""),
#                        fulltext,
#                        doc["date"])


# class ArchivesCollectionSpecification(CollectionSpecification):
#     """ Specification for the archives collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "archives", Language.English,
#             "text", ["text"],
#             # "spacy:en:ner",
#             "trftc_noisy:en:ner",
#             ["source", "page"],
#             None
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], doc["source"],
#                        "",
#                        doc["text"],
#                        doc.get("date", ""))


# class NcbrasCollectionSpecification(CollectionSpecification):
#     """ Specification for the NCBRAS collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "ncbras", Language.English,
#             "text", ["text", "title", "authors"],
#             # "spacy_sm:en:ner",
#             "trftc_noisy:en:ner",
#             ["volumes", "series"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], f"JNCBRAS vol. {doc['volumes']}",
#                        doc["title"],
#                        doc.get("text", ""),
#                        doc["date"])


# class ShunpaoCollectionSpecification(CollectionSpecification):
#     """ Specification for the Shunpao collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "shunpao", Language.Chinese,
#             "text", ["text", "title"],
#             "trftc_nopunct:zh:ner",
#             ["publication", "num", "page"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], "shunpao",
#                        doc.get("title", ""),
#                        doc.get("text", ""),
#                        doc["date"])


# class ZhangGangDiaryCollectionSpecification(CollectionSpecification):
#     """ Specification for the ZhangGangDiary collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "zhanggangdiary", Language.Chinese,
#             "entry", ["entry"],
#             "spacy:zh:ner",
#             ["source", "period", "nianhao", "year_number", "l_month", "intercal"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], doc["source"],
#                        "",
#                        doc["entry"],
#                        doc["date"])


# class DongfangzzCollectionSpecification(CollectionSpecification):
#     """ Specification for the Dongfangzz collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "dongfangzz", Language.Chinese,
#             "text", ["text", "title", "authors"],
#             "spacy:zh:ner",
#             ["publisher", "category", "volume", "issue", "source_type"],
#             "date_rdt"
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], doc.get("publisher", ""),
#                        doc.get("title", ""),
#                        doc.get("text", ""),
#                        doc.get("date", ""))


# class Kmt9kCollectionSpecification(CollectionSpecification):
#     """ Specification for the KMT9K collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "kmt9k", Language.Chinese,
#             "kmt_content", ["kmt_content", "kmt_name"],
#             "spacy:zh:ner",
#             [],
#             None
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], "KMT9K",
#                        doc["kmt_name"],
#                        doc["kmt_content"],
#                        "1993")


# class WaiguoZaihuaCollectionSpecification(CollectionSpecification):
#     """ Specification for the WaiguoZaihua collection.

#     """
#     def __init__(self):
#         super().__init__(
#             "waiguozaihua", Language.Chinese,
#             "wz_fulltext", ["wz_fulltext", "wz_company"],
#             "spacy:zh:ner",
#             ["wz_page"],
#             None
#         )

#     def as_basedoc(self, doc: dict[str, str]) -> BaseDoc:
#         return BaseDoc(doc["id"], "外國在華",
#                        doc["wz_company"],
#                        doc["wz_fulltext"],
#                        "1995")
