import logging
from fastapi import HTTPException
from .settings import solr_server
from .solr import SolrServer
from .specifications import load_specification_config, CollectionSpecification
# from .specifications import (Language, CollectionSpecification,
#                              ImhCollectionSpecification,
#                              WikibioCollectionSpecification,
#                              ReportsCollectionSpecification,
#                              ProquestCollectionSpecification,
#                              ArchivesCollectionSpecification,
#                              NcbrasCollectionSpecification,
#                              ShunpaoCollectionSpecification,
#                              ZhangGangDiaryCollectionSpecification,
#                              DongfangzzCollectionSpecification,
#                              Kmt9kCollectionSpecification,
#                              WaiguoZaihuaCollectionSpecification)


logger = logging.getLogger(__name__)

collections = load_specification_config()
# collections = {
#     "imh-en": ImhCollectionSpecification(Language.English),
#     "imh-zh": ImhCollectionSpecification(Language.Chinese),
#     "wikibio-en": WikibioCollectionSpecification(Language.English),
#     "wikibio-zh": WikibioCollectionSpecification(Language.Chinese),
#     "reports-en": ReportsCollectionSpecification(Language.English),
#     "reports-fr": ReportsCollectionSpecification(Language.French),
#     "proquest": ProquestCollectionSpecification(),
#     "archives": ArchivesCollectionSpecification(),
#     "ncbras": NcbrasCollectionSpecification(),
#     "shunpao": ShunpaoCollectionSpecification(),
#     "zhanggangdiary": ZhangGangDiaryCollectionSpecification(),
#     "dongfangzz": DongfangzzCollectionSpecification(),
#     "kmt9k": Kmt9kCollectionSpecification(),
#     "waiguozaihua": WaiguoZaihuaCollectionSpecification()
# }


def get_corpus(collection: str) -> CollectionSpecification:
    """Retrieve the collection specification."""
    collection = collections.get(collection, None)
    if collection is None:
        raise HTTPException(404, "Collection not found.")
    return collection


def list_corpora() -> list[str]:
    """List collection names."""
    return list(collections.keys())


def get_solr_server() -> SolrServer:
    """Retrieve the SolrServer instance."""
    return solr_server
